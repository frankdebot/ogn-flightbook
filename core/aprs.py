from time import time
import socket

from ogn.client import AprsClient
from ogn.client import settings as ognsettings


class AClient(AprsClient):
    """
    a AprsClient SubClass adding redis client & check dictionary members and a modified run method.
    Tne new run method add redis client && check dictionary args to callback function.
    """
    def __init__(self, aprs_user, aprs_filter='', settings=ognsettings, redis=None):
        AprsClient.__init__(self, aprs_user, aprs_filter, settings)
        self.redis = redis
        self.check = dict()

    def run(self, callback, timed_callback=lambda client: None, autoreconnect=False):
        while not self._kill:
            try:
                keepalive_time = time()
                while not self._kill:
                    if time() - keepalive_time > self.settings.APRS_KEEPALIVE_TIME:
                        self.logger.info('Send keepalive')
                        self.sock.send('#keepalive\n'.encode())
                        timed_callback(self)
                        keepalive_time = time()

                    # Read packet string from socket
                    packet_str = self.sock_file.readline().strip()

                    # A zero length line should not be return if keepalives are being sent
                    # A zero length line will only be returned after ~30m if keepalives are not sent
                    if len(packet_str) == 0:
                        self.logger.warning('Read returns zero length string. Failure.  Orderly closeout')
                        break
                    # Modified section here in callback.
                    callback(packet_str, self.redis, self.check)
            except ConnectionError:
                self.logger.error('ConnectionError', exc_info=True)
            except socket.error:
                self.logger.error('socket.error', exc_info=True)
            except UnicodeDecodeError:
                self.logger.error('UnicodeDecodeError', exc_info=True)

            if autoreconnect and not self._kill:
                self.connect()
            else:
                return
