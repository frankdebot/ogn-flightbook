from core.logbook import Logbook
import click

@click.command()
@click.argument('code')
@click.option('-d', '--date', help='use date in format like 2020_07_17 or 2020-07-17')
@click.option('-tz', '--time-zone', type=int, help='specific time_zone for logbook (pos or neg int)')
@click.option('-a', '--address', is_flag=True, help='show aircraft address in logbook')
@click.option('-nt', '--no-tow', is_flag=True, help='do not associate tow')
@click.option('-go', '--glider-only', is_flag=True, help='just show glider and associated tow')
def main(code, date, time_zone, address, no_tow, glider_only):
    """
    Display LogBook for a given OACI code
    """
    logbook = Logbook(date=date, code=code, tz=time_zone)
    logbook.create()
    logbook.print(show_add=address, no_tow=no_tow, glider_only=glider_only)


if __name__ == "__main__":
    """
    create a logbook and display it in console depending on the given options.
    """
    main()
