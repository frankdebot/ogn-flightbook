## Production setup
#### Serveur Provisioning:
Flightbook was developed for small architecture constraints. Any machine with at least 1 CPU and 2G RAM is enought.  
You can try to allocate an Amazon EC2 t2.small instance, Digital Ocean Basic Droplet and so on ...  
Ensure than server firewall is set-up to allow TCP port 22 (ssh) and TCP port 80 (http).  
Deployment has been planned for the latest Debian or Ubuntu distributions but it can fit with any up to date Linux system.  
You need docker, ansible, git and ssh client on your machine for deployment setup.  
Code portability for Windows OS is not considered.

#### Deploy
Once your server is working and your ssh keys are set, type on your machine:
```bash
git clone https://gitlab.com/lemoidului/ogn-flightbook.git
cd deploy
ansible-playbook playbook_debian10.yml -i <server_name>, --user=<server_user_name>
                    or
ansible-playbook playbook_ubuntu20.04.yml -i <server_name>, --user=<server_user_name>
```
Produce the web app on your local system:
```bash
cd ../web
./build.sh
```
Copy the web app dist directory on server:
```bash
scp -r app/dist <server_user_name>@<server_name>:/home/<server_user_name>/ogn-flightbook/web/app
```

#### Setting up
Login to server
```bash
ssh <server_user_namer>@<server_name>
```
Ensure than Redis Server is up and running
```bash
sudo systemctl redis-server start
sudo systemctl status redis-server
```
Populate devices database & airfields locations + timezone:
```bash
cd /home/<server_user_name>/ogn-flightbook
source ./venv/bin/activate
python initdb.py -ad
python initdb.py -acs
# you can watch possible cmd line options with:
python initdb.py --help
```
#### start services
Start Wsgy server (Gunicorn) & Nginx
```bash
sudo systemctl start gunicorn
sudo systemctl start nginx
# ensure that everything is ok with
sudo systemctl status gunicorn
sudo systemctl status nginx
```
Inside three differents tmux sessions, start micro-services: aprs, feed and consumer:
```bash
tmux
source venv/bin/activate
python tasks.py aprs
# switch to another tmux Windows with [CTRL] + b then c:
source venv/bin/activate
python tasks.py feed
# switch to another tmux Windows with [CTRL] + b then c:
source venv/bin/activate
python tasks.py cons
# detach from tmux with [CTRL] + b then d
```
That's all folks

## Development set up
```
TODO
```
## Architecture in depth
Redis is the central piece of architecture.  
Data structures used are List, Geozset & Stream. If you are new to Redis, you can read following docs:
 
 - [Redis data-types](https://redis.io/topics/data-types)
 - [Redis Geocoding](https://redis.io/commands/geoadd)
 - [Redis Stream](https://redis.io/topics/streams-intro)


### Redis as a Queue Manager
![Diag1](./pics/Diag1.png)
### Redis for Geocoding
#### Find closest Airfield:
![Diag2](./pics/Diag2.png)
#### Find surrounding beacons:
![Diag3](./pics/Diag3.png)
### Redis for streaming Data

## Scalability in mind
```
TODO
```

### Customize configuration
```
TODO
```
