import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    date: null,
    airfield: {},
  },
  mutations: {
    save (state, obj) {
      state.airfield = obj.airfield
      state.date = obj.date
    },
  },
  actions: {
  },
  modules: {
  }
})
