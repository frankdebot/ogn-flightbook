import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import LogBook from '../views/LogBook.vue'
import NotFound from '../views/NotFound.vue'
import CompSar from '../components/sar.vue'
import CompMap from '../components/map.vue'
import CompAirfield from '../components/Airfield.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/logbook/:code',
    name: 'LogBook1',
    component: LogBook
  },
  {
    path: '/logbook/:code/:date',
    name: 'LogBook2',
    component: LogBook
  },
  {
    path: '/sar/:address',
    name: 'Sar',
    component: CompSar
  },
  {
    path: '/map/:code',
    name: 'Map',
    component: CompMap
  },
  {
    path: '/airfield/:days',
    name: 'Airfield',
    component: CompAirfield
  },
  {
    path: '*',
    component: NotFound
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
