function format(sec, form){
    if (sec == null || sec == "undefined"){return sec}
    if (form === "hms" || form === "hm"){
            let date_str = new Date(sec*1000).toISOString()
            if (form === "hm"){return date_str.substr(11, 5)}
            else {return date_str.substr(11, 8) + " (" + Math.round(sec*1/36)+ " cent)"}
    }
    else { return sec}
}

export function create_logbook(json) {
	if (json == null || json == "undefined") {json = {airfield: {}, flights: []}}
	let devices = json.devices;
	let flights = json.flights;
	let flight_warning = false
	let warning = false
	let warningdict = {}
	for (var idx=0; idx < flights.length; idx++){
		let flight = flights[idx];
		flight.device = devices[flight.device];
		flight.glider = (flight.device.aircraft_type === 1)
        flight.duration_hm = format(flight.duration, "hm");
        flight.duration_hms = format(flight.duration, "hms");
		flight.tow = flights[flight.tow];
		flight_warning = false;
		(flight.glider && (flight.start!==null)) ? flight_warning = false : '';
		(flight.glider && (flight.stop===null)) ? flight_warning = true : '';
		flight.warning = flight_warning && flight.warn
		warningdict[flight.device.address] = flight.warning;
	}
	for (let k in warningdict){
		if (warningdict[k]) {
			warning = true
			break
		}
	}
	json.warning = warning
	return json
}

export function nullable(obj, key) {return key.split(".").reduce(function(o, x) {return (typeof o == "undefined" || o === null) ? null : o[x];}, obj)}