import logging

from flask import Flask, jsonify
import redis

from core.live import address as laddress, path as lpath, fleet as lfleet, positions
from web.wlbook import WLogbook
from utils.db import Db
import setting

app = Flask(__name__)
pool = redis.ConnectionPool(host=setting.REDIS_HOST, port=setting.REDIS_PORT, db=setting.REDIS_DB,
                            decode_responses=True, encoding="utf-8")
red = redis.Redis(connection_pool=pool)


@app.route('/api/logbook/<code>', defaults={'date': None}, strict_slashes=False)
@app.route('/api/logbook/<code>/<date>', strict_slashes=False)
def make_logbook(code, date=None):
    db = Db(path=setting.SQLITE_PATH)
    logbook = WLogbook(date=date, code=code.upper(), red=red, db=db)
    logbook.create()
    logbook.print(show_add=True)
    response = jsonify(logbook.to_json())
    response.headers.add('Access-Control-Allow-Origin', '*')
    db.close()
    return response


@app.route('/api/autocomp/<query>', strict_slashes=False)
def autocomplete(query):
    db = Db(path=setting.SQLITE_PATH)
    response = list()
    elts = db.search_airfield(query=query, limit=5)
    id_ = 0
    for elt in elts:
        response.append({"id": id_, "code": elt[0], "name": elt[1], "elevation": elt[2], "tz": elt[3]})
        id_ += 1

    response = jsonify(response)
    response.headers.add('Access-Control-Allow-Origin', '*')
    db.close()
    return response


@app.route('/api/airfield', defaults={'days': None}, strict_slashes=False)
@app.route('/api/airfield/<int:days>', strict_slashes=False)
def search_null(days):
    """
    helper for finding potential airfields unregister in system (OACI code null and aircraft stopped)
    :param days: int, nb of days. starting search from today - nb of days
    :return: json aircraft(s) array
    """
    db = Db(path=setting.SQLITE_PATH)
    addresses = db.search_null(minus_day=days)
    db.close()
    gspnull = [elt for elt in positions(red=red, addresses=addresses) if elt['gsp'] == 0]
    response = jsonify(gspnull)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/api/live/address/<address>')
def live_address(address):
    response = laddress(red=red, address_=address)

    response = jsonify(response)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/api/live/path/<address>')
def live_path(address):
    response = jsonify(lpath(red=red, address_=address))
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/api/live/fleet/<code>')
def live_fleet(code):
    response = jsonify(lfleet(red=red, code=code))
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    app.run(debug=True)
